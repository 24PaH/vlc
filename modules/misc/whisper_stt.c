/*****************************************************************************
 * whisper_stt.c : whisper.cpp implementation for automatic subtitles
 *****************************************************************************
 * Copyright © 2023 VLC authors and VideoLAN
 *
 * Authors: Hamza Parnica <hparnica@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/

#ifdef HAVE_CONFIG_H
# include "config.h"
#endif

#include <vlc_common.h>
#include <vlc_filter.h>
#include <vlc_configuration.h>

#include <whisper.h>

#include "whisper_stt.h"


static int queue_fill(vlc_whisper_t *p_whisper, block_t *p_block)
{
    float *p_buffer = (float *)p_block->p_buffer;
    float *p_buffer_queue = p_whisper->p_buffer_queue;

    /* cutting block if it is bigger than buffer left space */
    unsigned i_samples_to_write = p_block->i_nb_samples - p_whisper->i_written_samples;
    unsigned i_new_size = p_whisper->i_buffer_size + i_samples_to_write;
    if (i_new_size >= p_whisper->i_buffer_max_size) {
        i_new_size = p_whisper->i_buffer_max_size;
        i_samples_to_write = p_whisper->i_buffer_max_size - p_whisper->i_buffer_size;
    }

    /* populating buffer queue with current buffer */
    memmove(p_buffer_queue + p_whisper->i_buffer_size, p_buffer + p_whisper->i_written_samples, i_samples_to_write * sizeof(float));
    p_whisper->i_buffer_size = i_new_size;
    p_whisper->i_written_samples += i_samples_to_write;

    if (i_new_size == p_whisper->i_buffer_max_size) {
        return 1;
    }

    return 0;
}

static int transcribe(vlc_whisper_t *p_whisper)
{
    p_whisper->params.duration_ms = MS_FROM_VLC_TICK(vlc_tick_from_samples(p_whisper->i_buffer_size, WHISPER_SAMPLE_RATE));
    msg_Info(p_whisper, "duration (ms): %d, nb samples: %d", p_whisper->params.duration_ms, p_whisper->i_buffer_size);

    if(whisper_full(p_whisper->p_ctx, p_whisper->params, p_whisper->p_buffer_queue, p_whisper->i_buffer_size) != 0) {
        msg_Err(p_whisper, "failed to process audio");
        return VLC_EGENERIC;
    }

    const int i_nb_segments = whisper_full_n_segments(p_whisper->p_ctx);
    for (int i = 0; i < i_nb_segments; ++i) {
        const char *text = whisper_full_get_segment_text(p_whisper->p_ctx, i);

        vlc_tick_t i_t0 = VLC_TICK_FROM_MS(whisper_full_get_segment_t0(p_whisper->p_ctx, i)*10);
        vlc_tick_t i_t1 = VLC_TICK_FROM_MS(whisper_full_get_segment_t1(p_whisper->p_ctx, i)*10);

        i_t0 = p_whisper->i_buffer_timestamp + i_t0;
        i_t1 = p_whisper->i_buffer_timestamp + i_t1;

        /* avoid subtitles overlapping between two transcriptions (tweak) */
        if (i_t0 < p_whisper->i_segment_old_timestamp)
            i_t0 = p_whisper->i_segment_old_timestamp;
        if (i_t0 < i_t1) {
            p_whisper->i_segment_old_timestamp = i_t1;

            char psz_t0[MSTRTIME_MAX_SIZE];
            char psz_t1[MSTRTIME_MAX_SIZE];
            vlc_tick_to_str(psz_t0, i_t0);
            vlc_tick_to_str(psz_t1, i_t1);

            unsigned i_t0_ms = MS_FROM_VLC_TICK(i_t0) % 1000;
            unsigned i_t1_ms = MS_FROM_VLC_TICK(i_t1) % 1000;

            msg_Info(p_whisper, "[%s,%03d --> %s,%03d] %s", psz_t0, i_t0_ms, psz_t1, i_t1_ms, text);

            fprintf(p_whisper->p_srt, "%d\n00:%s,%03d --> 00:%s,%03d\n%s\n\n", p_whisper->i_srt_count, psz_t0, i_t0_ms, psz_t1, i_t1_ms, text);
            p_whisper->i_srt_count += 1;
        }
    }

    return VLC_SUCCESS;
}

int vlc_whisper_ProcessBlock(vlc_whisper_t *p_whisper, block_t *p_block)
{
    float *p_buffer_queue = p_whisper->p_buffer_queue;

    if (p_whisper->i_buffer_timestamp == 0) {
        p_whisper->i_buffer_timestamp = p_block->i_pts - VLC_TICK_0;
    }

    /* nb of samples that have been copied from p_block */
    p_whisper->i_written_samples = 0;

    while (queue_fill(p_whisper, p_block)) {
        transcribe(p_whisper);

        unsigned i_keep_samples = samples_from_vlc_tick(p_whisper->i_keep, WHISPER_SAMPLE_RATE);

        /* rolling last KEEP_MS of audio from previous chunk to avoid word boundary issues */
        memmove(p_buffer_queue, p_buffer_queue + p_whisper->i_buffer_size - i_keep_samples, i_keep_samples * sizeof(float));

        p_whisper->i_buffer_size = i_keep_samples;
        p_whisper->i_buffer_timestamp = p_block->i_pts - VLC_TICK_0 - p_whisper->i_keep + vlc_tick_from_samples(p_whisper->i_written_samples, WHISPER_SAMPLE_RATE);
    }

    return VLC_SUCCESS;
}

void vlc_whisper_Drain(vlc_whisper_t *p_whisper)
{
    /* transcribing last audio chunk */
    transcribe(p_whisper);

    vlc_whisper_Flush(p_whisper); // needed ?
}

void vlc_whisper_Flush(vlc_whisper_t *p_whisper)
{
    p_whisper->i_buffer_size = 0;
    p_whisper->i_buffer_timestamp = 0;
    p_whisper->i_segment_old_timestamp = 0;
}

void vlc_whisper_Release(vlc_whisper_t *p_whisper)
{
    free(p_whisper->p_buffer_queue);
    whisper_free(p_whisper->p_ctx);
    fclose(p_whisper->p_srt);
    vlc_object_delete(p_whisper);
}

vlc_whisper_t *vlc_whisper_Create(vlc_object_t *p_obj)
{
    vlc_whisper_t *p_whisper = vlc_object_create(VLC_OBJECT(p_obj), sizeof(*p_whisper));
    if (unlikely(!p_whisper))
        return NULL;

    char* model_path = config_GetSysPath (VLC_PKG_DATA_DIR, NULL);
    if (unlikely(!model_path))
        return NULL;
    model_path = realloc(model_path, strlen(model_path) + strlen(ML_MODEL) + 1);
    if (unlikely(!model_path))
        return NULL;
    strcat(model_path, ML_MODEL);

    struct whisper_context *ctx = whisper_init_from_file(model_path);
    //struct whisper_context *ctx = whisper_init_from_file("../../Downloads/ggml-base.en.bin");
    p_whisper->p_ctx = ctx;
    if (unlikely(!p_whisper->p_ctx)) {
        vlc_object_delete(p_whisper);
        return NULL;
    }

    p_whisper->i_step = VLC_TICK_FROM_MS(STEP_MS);
    p_whisper->i_keep = VLC_TICK_FROM_MS(KEEP_MS);

    p_whisper->i_buffer_max_size = samples_from_vlc_tick(p_whisper->i_step, WHISPER_SAMPLE_RATE);
    p_whisper->p_buffer_queue = (float *) malloc(p_whisper->i_buffer_max_size * sizeof(float));
    msg_Info(p_whisper, "size of buffer queue: %dx%ld bytes", p_whisper->i_buffer_max_size, sizeof(float));
    if (unlikely(!p_whisper->p_buffer_queue)) {
        whisper_free(p_whisper->p_ctx);
        vlc_object_delete(p_whisper);
        return NULL;
    }
    p_whisper->i_buffer_size = 0;
    p_whisper->i_buffer_timestamp = 0;
    p_whisper->i_segment_old_timestamp = 0;

    p_whisper->params = whisper_full_default_params(WHISPER_SAMPLING_GREEDY);
    p_whisper->params.language = "en"; // for testing purposes
    p_whisper->params.token_timestamps = 1;
    p_whisper->params.max_len = SEGMENT_LEN;
    //p_whisper->params.no_context = 0; // causes repeating pattern "hallucination"
    //p_whisper->params.audio_ctx = 768;

    p_whisper->p_srt = fopen("../trolling.srt", "a");
    p_whisper->i_srt_count = 1;

    free(model_path);

    return p_whisper;
}
