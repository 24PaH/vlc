/*****************************************************************************
 * whisper_stt.h : whisper_stt abstraction
 *****************************************************************************
 * Copyright © 2023 VLC authors and VideoLAN
 *
 * Authors: Hamza Parnica <hparnica@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/

#ifndef VLC_WHISPER_STT_H
#define VLC_WHISPER_STT_H


#define STEP_MS 18000
#define KEEP_MS 400

#define SEGMENT_LEN 60

#define ML_MODEL "/ggml-base.bin"

typedef struct
{
    struct vlc_object_t obj;

    struct whisper_context *p_ctx;
    struct whisper_full_params params;

    /* step between two inferences */
    vlc_tick_t i_step;
    vlc_tick_t i_keep;

    float *p_buffer_queue;
    unsigned i_buffer_size;
    unsigned i_buffer_max_size;
    unsigned i_written_samples;
    vlc_tick_t i_buffer_timestamp;
    vlc_tick_t i_segment_old_timestamp;

    FILE *p_srt;
    unsigned i_srt_count;
} vlc_whisper_t;

vlc_whisper_t *vlc_whisper_Create(vlc_object_t *p_obj);
void vlc_whisper_Release(vlc_whisper_t *p_whisper);
int vlc_whisper_ProcessBlock(vlc_whisper_t *p_whisper, block_t *p_block);
void vlc_whisper_Flush(vlc_whisper_t *p_whisper);
void vlc_whisper_Drain(vlc_whisper_t *p_whisper);

#endif // VLC_WHISPER_STT_H
