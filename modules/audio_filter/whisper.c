/*****************************************************************************
 * whisper.c : whisper.cpp implementation for automatic subtitles
 *****************************************************************************
 * Copyright © 2023 VLC authors and VideoLAN
 *
 * Authors: Hamza Parnica <hparnica@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/

#ifdef HAVE_CONFIG_H
# include "config.h"
#endif

#include <vlc_common.h>
#include <vlc_aout.h>
#include <vlc_filter.h>
#include <vlc_plugin.h>

#include <whisper.h>
#include "../misc/whisper_stt.h"


typedef struct {
    vlc_whisper_t *p_whisper;
} filter_sys_t;


static void Flush(filter_t *p_filter) // called on seek action
{
    filter_sys_t *p_sys = p_filter->p_sys;
    vlc_whisper_Flush(p_sys->p_whisper);
}

static block_t *Drain(filter_t *p_filter)
{
    filter_sys_t *p_sys = p_filter->p_sys;
    vlc_whisper_Drain(p_sys->p_whisper);

    return NULL;
}

static block_t *Process(filter_t *p_filter, block_t *p_block)
{
    filter_sys_t *p_sys = p_filter->p_sys;
    vlc_whisper_ProcessBlock(p_sys->p_whisper, p_block);

    return NULL;
}

static void Close(filter_t *obj)
{
    filter_t *p_filter = (filter_t *)obj;
    filter_sys_t *p_sys = p_filter->p_sys;

    vlc_whisper_Release(p_sys->p_whisper);
    vlc_obj_free(obj, p_sys);
}

static int Open(vlc_object_t *obj)
{
    filter_t *p_filter = (filter_t *)obj;

    filter_sys_t *p_sys = p_filter->p_sys = vlc_obj_malloc(VLC_OBJECT(p_filter), sizeof(*p_sys));
    if (unlikely(!p_sys))
        return VLC_ENOMEM;

    p_filter->fmt_in.audio.i_format = VLC_CODEC_FL32;
    p_filter->fmt_in.audio.i_rate = WHISPER_SAMPLE_RATE;
    p_filter->fmt_in.audio.i_physical_channels = 1;
    msg_Info(p_filter, "physical channels: %d", p_filter->fmt_in.audio.i_physical_channels);
    aout_FormatPrepare(&p_filter->fmt_in.audio);
    p_filter->fmt_out.audio = p_filter->fmt_in.audio;

    static const struct vlc_filter_operations filter_ops =
    {
        .filter_audio = Process, .flush = Flush,
        .drain_audio = Drain, .close = Close,
    };
    p_filter->ops = &filter_ops;

    p_sys->p_whisper = vlc_whisper_Create(obj);

    p_filter->owner.audio->stt.on_new_format(p_filter, NULL);

    return VLC_SUCCESS;
}

vlc_module_begin ()
    set_shortname (N_("Whisper"))
    set_description (N_("Whisper live automatic subtitles"))
    set_subcategory (SUBCAT_AUDIO_AFILTER)
    set_capability ("audio stt", 10)
    set_callback( Open )
vlc_module_end ()
