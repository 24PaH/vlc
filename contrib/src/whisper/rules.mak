WHISPER_GITHASH := 9b144188639f09a5be5e751d86bd334fbce67070
WHISPER_VERSION := git-$(WHISPER_GITHASH)
WHISPER_GITURL := $(GITHUB)/ggerganov/whisper.cpp

PKGS += whisper

ifeq ($(call need_pkg,"whisper"),)
PKGS_FOUND += whisper
endif

$(TARBALLS)/whisper-$(WHISPER_GITHASH).tar.xz:
	$(call download_git,$(WHISPER_GITURL),,$(WHISPER_GITHASH))

.sum-whisper: whisper-$(WHISPER_GITHASH).tar.xz
	$(call check_githash,$(WHISPER_GITHASH))
	touch $@

whisper: whisper-$(WHISPER_GITHASH).tar.xz .sum-whisper
	$(UNPACK)
	$(MOVE)

WHISPER_CONF := -DWHISPER_BUILD_EXAMPLES=OFF -DWHISPER_BUILD_TESTS=OFF

.whisper: whisper
	$(CMAKECLEAN)
	$(HOSTVARS) $(CMAKE) $(WHISPER_CONF)
	+$(CMAKEBUILD)
	$(CMAKEINSTALL)
	touch $@
