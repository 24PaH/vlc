/*****************************************************************************
 * stt.c : speech to text
 *****************************************************************************
 * Copyright (C) 2023 VLC authors and VideoLAN
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/

#ifdef HAVE_CONFIG_H
# include "config.h"
#endif

#include <assert.h>

#include <vlc_common.h>
#include <vlc_configuration.h>
#include <vlc_modules.h>
#include <vlc_aout.h>
#include "aout_internal.h"


struct vlc_aout_stt_t
{
    char *name;
    filter_t *filter;
    config_chain_t *cfg;
    aout_filters_t *conv_chain;

    struct vlc_aout_stt_callbacks cbs;
    void *cbs_ctx;
};

static void on_new_format(filter_t *filter, const es_format_t *fmt)
{
    vlc_aout_stt_t *stt = filter->owner.sys;
    stt->cbs.on_new_format(fmt, stt->cbs_ctx);
}

static void on_subpicture(filter_t *filter, subpicture_t *subpic)
{
    vlc_aout_stt_t *stt = filter->owner.sys;
    stt->cbs.on_subpicture(subpic, stt->cbs_ctx);
}

vlc_aout_stt_t *vlc_aout_stt_Create(vlc_object_t *obj, const char *chain,
                                    const audio_sample_format_t *input_fmt,
                                    const struct vlc_aout_stream_cfg *cfg)
{
    vlc_aout_stt_t *stt = malloc(sizeof(*stt));
    if (unlikely(stt == NULL))
        return NULL;

    free(config_ChainCreate(&stt->name, &stt->cfg, chain));
    if (stt->name == NULL)
    {
        free(stt);
        return NULL;
    }

    stt->cbs = cfg->stt_cbs;
    stt->cbs_ctx = cfg->stt_ctx;

    static const struct filter_audio_callbacks audio_cbs = {
        .stt = {
            .on_new_format = on_new_format,
            .on_subpicture = on_subpicture,
        },
    };

    const filter_owner_t owner = {
        .audio = &audio_cbs,
        .sys = stt,
    };

    audio_sample_format_t outfmt = *input_fmt;
    stt->filter = aout_filter_Create(obj, &owner, "audio stt", stt->name,
                                     input_fmt,
                                     &outfmt, stt->cfg, false);
    if (stt->filter == NULL)
        goto error;

    stt->conv_chain = aout_FiltersNewWithClock(obj, NULL, input_fmt,
                                               &stt->filter->fmt_out.audio,
                                               NULL);
    if (stt->conv_chain == NULL)
        goto error;

    return stt;

error:
    // TODO clean
    fprintf(stderr, "error in stt.c\n");
    return NULL;
}

void vlc_aout_stt_Process(vlc_aout_stt_t *stt, block_t *block)
{
    block = aout_FiltersPlay(stt->conv_chain, block, 1.0f);

    if (block != NULL)
    {
        block_t *null_block = stt->filter->ops->filter_audio(stt->filter, block);
        assert(!null_block);
    }
}

void vlc_aout_stt_Drain(vlc_aout_stt_t *stt)
{
    block_t *null_block = stt->filter->ops->drain_audio(stt->filter);
    assert(!null_block);
}

void vlc_aout_stt_Flush(vlc_aout_stt_t *stt)
{
    stt->filter->ops->flush(stt->filter);
}

void vlc_aout_stt_Destroy(vlc_aout_stt_t *stt)
{
}
